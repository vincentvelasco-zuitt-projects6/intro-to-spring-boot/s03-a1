package com.velascob142.s02.s02app.models;

import javax.persistence.*;


@Entity
@Table(name="users")
public class User {

    @Id
    @GeneratedValue
    private Long id; // primary key
    @Column
    private String username;
    @Column
    private String password;


    // Constructors
    //Data: title, content
    public User(){}

    public User(String title, String content){
        this.username = title;
        this.password = content;
    }

    // Getters & Setters
    public String getUsername(){
        return username;
    }

    public String getPassword(){
        return password;
    }


    public void setUsername(String newUsername){
        this.username = newUsername;
    }

    public void setPassword(String newPassword){
        this.password = newPassword;
    }
}
