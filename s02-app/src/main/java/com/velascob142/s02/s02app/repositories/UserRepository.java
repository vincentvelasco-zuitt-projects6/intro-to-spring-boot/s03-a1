package com.velascob142.s02.s02app.repositories;

import com.velascob142.s02.s02app.models.Post;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<Post, Object> {
}
